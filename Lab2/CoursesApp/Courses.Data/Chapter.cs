﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public class Chapter
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsPremium { get; set; }
        [DataMember]
        public int CourseId { get; set; }
        public ICollection<Lesson> Lessons { get; set; }
    }
}
