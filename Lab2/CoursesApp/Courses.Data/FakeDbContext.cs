﻿using Courses.Data.Implementation;
using Courses.Data.Interfaces;

namespace Courses.Data
{
    public class FakeDbContext
    {
        public IChapterRepository ChapterRepository => new ChapterRepository("Chapter.data");
        public ICourseRepository CourseRepository => new CourseRepository("Course.data");
        public ILessonRepository LessonRepository => new LessonRepository("Lessons.data");
        public ILessonTypeRepository LessonTypeRepository => new LessonTypeRepository("LessonType.data");
        public IUserCourseRepository UserCourseRepository => new UserCourseRepository("UserCourse.data");
        public IUserRepository UserRepository => new UserRepository("Users.data");
        //public void Seed()
        //{
        //    LessonTypes = new List<LessonType>()
        //    {
        //        new LessonType()
        //        {
        //            Id = 1,
        //            Name = "Text"
        //        },
        //        new LessonType()
        //        {
        //            Id = 2,
        //            Name = "Video",
        //        },
        //        new LessonType()
        //        {
        //            Id = 3,
        //            Name = "Interactive"
        //        }
        //    };
        //    Lessons = new List<Lesson>()
        //    {
        //        new Lesson()
        //        {
        //            Name = "What is wpf?",
        //            Id = 1,
        //            IsPremium = false,
        //            Content = null,
        //            LessonType = LessonTypes.ElementAt(0)
        //        },
        //        new Lesson()
        //        {
        //            Name = "How to build your first desktop application?",
        //            IsPremium = false,
        //            Id = 2,
        //            Content = null,
        //            LessonType = LessonTypes.ElementAt(0)
        //        },
        //        new Lesson()
        //        {
        //            Id = 3,
        //            Name = "What is it 'binding'?",
        //            IsPremium = false,
        //            Content = null,
        //            LessonType = LessonTypes.ElementAt(0)
        //        },
        //        new Lesson()
        //        {
        //            Id = 4,
        //            Name = "All cool wpf stuff",
        //            Content = null,
        //            IsPremium = false,
        //            LessonType = LessonTypes.ElementAt(0)
        //        }
        //    };
        //    Chapters = new List<Chapter>()
        //    {
        //        new Chapter()
        //        {
        //            Id = 1,
        //            IsPremium=false,
        //            Name = "WPF basics",
        //            Lessons = new List<Lesson> ()
        //            {
        //                Lessons.ElementAt(0),
        //                Lessons.ElementAt(1),
        //            }
        //        },
        //        new Chapter()
        //        {
        //            Id = 2,
        //            IsPremium = false,
        //            Name = "WPF binding",
        //            Lessons = new List<Lesson>()
        //            {
        //                Lessons.ElementAt(2),
        //            }
        //        },
        //        new Chapter()
        //        {
        //            Id = 3,
        //            IsPremium = false,
        //            Name = "WPF advanced",
        //            Lessons = new List<Lesson>()
        //            {
        //                Lessons.ElementAt(3)
        //            }
        //        }
        //    };
        //    Courses = new List<Course>()
        //    {
        //        new Course()
        //        {
        //            Id = 1,
        //            Chapters = new List<Chapter>()
        //            {
        //                Chapters.ElementAt(0),
        //                Chapters.ElementAt(1),
        //                Chapters.ElementAt(2)
        //            },
        //            Name = "WPF",
        //            Description = "Course created for developers whose want to increase their knowledge about wpf",
        //            IsPremium = false,
        //        }
        //    };

        //    Users = new List<User>()
        //    {
        //        new User()
        //        {
        //            Id = 1,
        //            BirthDate = DateTime.Now,
        //            Courses = new List<Course>()
        //            {
        //                Courses.ElementAt(0)
        //            },
        //            Email = "bomjyk@gmail.com",
        //            FirstName = "Oleksandr",
        //            LastName = "Burlakov",
        //            Password = "qwer1234",
        //            Role = Roles.Admin.ToString(),
        //        }
        //    };
        //}
    }
}
