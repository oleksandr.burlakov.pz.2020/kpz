﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

namespace Courses.Data.Implementation
{
    public class ChapterRepository : GeneralRepository<Chapter>, IChapterRepository
    {
        public ChapterRepository(string savePath) : base(savePath, new BinaryFormatter())
        {
        }

        public ICollection<Chapter> GetAll()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), base._savePath);
            ICollection<Chapter> list = new List<Chapter>();
            if (File.Exists(path))
            {
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas()))
                    {
                        var ser = new DataContractSerializer(typeof(ICollection<Chapter>));
                        list = (ICollection<Chapter>)ser.ReadObject(reader, true);
                    }
                }
            }
            return list;
        }

        public ICollection<Chapter> GetByCourseId(int courseId)
        {
            return GetAll()
                .Where(c => c.CourseId == courseId)
                .ToList();
        }

        public void Remove(int id)
        {
            var chapters = GetAll()
                .Where(c => c.Id != id)
                .ToList();

            Serialize(chapters);
        }

        public void RemoveRange(ICollection<Chapter> chaptersToRemove)
        {
            var chapters = GetAll()
                .Where(c => !chaptersToRemove.Any(cr => cr.Id == c.Id))
                .ToList();

            Serialize(chapters);
        }

        public void SaveAll(ICollection<Chapter> items)
        {
            var allItems = this.GetAll()
                .Where(i => !items.Any(el => el.Id == i.Id))
                .ToList();
            allItems.AddRange(items);
            Serialize(allItems);
        }

        private void Serialize(ICollection<Chapter> items)
        {

            var path = Path.Combine(Directory.GetCurrentDirectory(), base._savePath);
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                var ser = new DataContractSerializer(typeof(ICollection<Chapter>));
                ser.WriteObject(stream, items);
            }
        }
    }
}
