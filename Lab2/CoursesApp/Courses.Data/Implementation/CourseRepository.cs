﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

namespace Courses.Data.Implementation
{
    public class CourseRepository : GeneralRepository<Course>, ICourseRepository
    {
        public CourseRepository(string savePath) : base(savePath, new BinaryFormatter())
        {
        }

        public ICollection<Course> GetAll()
        {
            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), base._savePath);
                ICollection<Course> list = new List<Course>();
                if (File.Exists(path))
                {
                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas()))
                        {
                            var ser = new DataContractSerializer(typeof(ICollection<Course>));
                            list = (ICollection<Course>)ser.ReadObject(reader, true);
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                return new List<Course>();
            }
        }

        public void SaveAll(ICollection<Course> items)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), base._savePath);
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                var ser = new DataContractSerializer(typeof(ICollection<Course>));
                ser.WriteObject(stream, items);
            }
        }

        public void RemoveById(int id)
        {
            var items = this.GetAll()
                .Where(i => i.Id != id)
                .ToList();
            this.SaveAll(items);
        }

        public Course GetById(int id)
        {
            return this.GetAll()
                .FirstOrDefault(i => i.Id == id);
        }

        public void Save(Course course)
        {
            var items = this.GetAll()
                .Where(i => i.Id != course.Id)
                .ToList();
            items.Add(course);
            this.SaveAll(items);
        }
    }
}
