﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization;

namespace Courses.Data.Implementation
{
    public abstract class GeneralRepository<T> : IGeneralRepository<T> where T : class
    {
        protected readonly string _savePath;
        protected IFormatter _formatter;
        public GeneralRepository(string savePath, IFormatter formatter)
        {
            this._savePath = savePath;
            this._formatter = formatter;
        }
        public ICollection<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public void SaveAll(ICollection<T> items)
        {
            throw new NotImplementedException();
        }
    }
}
