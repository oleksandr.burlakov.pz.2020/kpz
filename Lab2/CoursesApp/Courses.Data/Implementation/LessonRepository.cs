﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization.Formatters.Binary;

namespace Courses.Data.Implementation
{
    public class LessonRepository : GeneralRepository<Lesson>, ILessonRepository
    {
        public LessonRepository(string savePath) : base(savePath, new BinaryFormatter())
        {
        }

        public ICollection<Lesson> GetAll()
        {
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), base._savePath)))
            {
                using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Open, FileAccess.Read))
                {
                    return (List<Lesson>)base._formatter.Deserialize(stream);
                }
            }
            return new List<Lesson>();
        }

        public void SaveAll(ICollection<Lesson> items)
        {
            using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Create, FileAccess.Write))
            {
                base._formatter.Serialize(stream, items);
            }
        }
    }
}
