﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization.Formatters.Binary;

namespace Courses.Data.Implementation
{
    public class LessonTypeRepository : GeneralRepository<LessonType>, ILessonTypeRepository
    {
        public LessonTypeRepository(string savePath) : base(savePath, new BinaryFormatter())
        {
        }

        public ICollection<LessonType> GetAll()
        {
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), base._savePath)))
            {
                using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Open, FileAccess.Read))
                {
                    return (List<LessonType>)base._formatter.Deserialize(stream);
                }
            }
            return new List<LessonType>();
        }

        public void SaveAll(ICollection<LessonType> items)
        {
            using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Create, FileAccess.Write))
            {
                base._formatter.Serialize(stream, items);
            }
        }
    }
}
