﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization.Formatters.Binary;

namespace Courses.Data.Implementation
{
    public class UserCourseRepository : GeneralRepository<UserCourse>, IUserCourseRepository
    {
        public UserCourseRepository(string savePath) : base(savePath, new BinaryFormatter())
        {
        }

        public ICollection<UserCourse> GetAll()
        {
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), base._savePath)))
            {
                using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Open, FileAccess.Read))
                {
                    return (List<UserCourse>)base._formatter.Deserialize(stream);
                }
            }
            return new List<UserCourse>();
        }

        public void SaveAll(ICollection<UserCourse> items)
        {
            using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Create, FileAccess.Write))
            {
                base._formatter.Serialize(stream, items);
            }
        }
    }
}
