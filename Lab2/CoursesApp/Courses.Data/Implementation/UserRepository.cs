﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization.Formatters.Binary;

namespace Courses.Data.Implementation
{
    public class UserRepository : GeneralRepository<User>, IUserRepository
    {
        public UserRepository(string savePath) : base(savePath, new BinaryFormatter())
        {
        }

        public ICollection<User> GetAll()
        {
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), base._savePath)))
            {
                using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Open, FileAccess.Read))
                {
                    return (List<User>)base._formatter.Deserialize(stream);
                }
            }
            return new List<User>();
        }

        public User GetByLogin(string login)
        {
            throw new NotImplementedException();
        }

        public void SaveAll(ICollection<User> items)
        {
            using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), base._savePath), FileMode.Create, FileAccess.Write))
            {
                base._formatter.Serialize(stream, items);
            }
        }
    }
}
