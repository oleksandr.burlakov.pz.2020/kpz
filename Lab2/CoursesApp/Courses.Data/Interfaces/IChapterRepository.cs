﻿using System;
namespace Courses.Data.Interfaces
{
    public interface IChapterRepository : IGeneralRepository<Chapter>
    {
        public void Remove(int Id);
        public void RemoveRange(ICollection<Chapter> chapters);
        public ICollection<Chapter> GetByCourseId(int courseId);
    }
}
