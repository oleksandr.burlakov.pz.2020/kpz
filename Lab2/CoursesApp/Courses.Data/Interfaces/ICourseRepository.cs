﻿namespace Courses.Data.Interfaces
{
    public interface ICourseRepository : IGeneralRepository<Course>
    {
        public void RemoveById(int id);
        public Course GetById(int id);
        public void Save(Course course);
    }
}
