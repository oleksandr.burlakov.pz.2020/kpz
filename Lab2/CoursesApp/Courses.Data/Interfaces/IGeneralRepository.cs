﻿namespace Courses.Data.Interfaces
{
    public interface IGeneralRepository<T> where T : class
    {
        public ICollection<T> GetAll();
        public void SaveAll(ICollection<T> items);
    }
}
