﻿namespace Courses.Data.Interfaces
{
    public interface ILessonRepository : IGeneralRepository<Lesson>
    {
    }
}
