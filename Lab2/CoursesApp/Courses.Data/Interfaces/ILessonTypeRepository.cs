﻿namespace Courses.Data.Interfaces
{
    public interface ILessonTypeRepository : IGeneralRepository<LessonType>
    {
    }
}
