﻿namespace Courses.Data.Interfaces
{
    public interface IUserCourseRepository : IGeneralRepository<UserCourse>
    {
    }
}
