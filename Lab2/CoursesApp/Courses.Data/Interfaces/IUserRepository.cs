﻿namespace Courses.Data.Interfaces
{
    public interface IUserRepository : IGeneralRepository<User>
    {
        public User GetByLogin(string login);
    }
}
