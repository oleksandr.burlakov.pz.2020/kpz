﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public class Lesson
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int LessonTypeId { get; set; }
        [DataMember]
        public bool IsPremium { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public int ChapterId { get; set; }
        public Chapter Chapter { get; set; }
        public LessonType LessonType { get; set; }
    }
}
