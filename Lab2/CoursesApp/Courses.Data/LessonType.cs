﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public class LessonType
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
