﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public class UserCourse
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int CourseId { get; set; }
    }
}
