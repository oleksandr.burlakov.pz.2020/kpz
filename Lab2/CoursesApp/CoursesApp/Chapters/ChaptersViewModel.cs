﻿using Courses.Data;
using Courses.Data.Implementation;
using Courses.Data.Interfaces;
using CoursesApp.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CoursesApp.Chapters
{
    public class ChaptersViewModel : INotifyPropertyChanged
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IChapterRepository _chapterRepository;
        public ChaptersViewModel()
        {
            _courseRepository = new CourseRepository("course.txt");
            _chapterRepository = new ChapterRepository("chapter.txt");
            _courses = new ObservableCollection<Course>(_courseRepository.GetAll());
            _chapters = new ObservableCollection<Chapter>();
        }

        private ObservableCollection<Chapter> _chapters;

        public ObservableCollection<Chapter> Chapters
        {
            get { return _chapters; }
            set { 
                _chapters = value;
                OnPropertyChanged();
            }
        }


        private ObservableCollection<Course> _courses;

        public ObservableCollection<Course> Courses
        {
            get { return _courses; }
            set { 
                _courses = value;
                OnPropertyChanged();
            }
        }

        private Course _selectedCourse;

        public Course SelectedCourse
        {
            get { return _selectedCourse; }
            set { 
                _selectedCourse = value;
                Chapters = new ObservableCollection<Chapter>(_chapterRepository.GetAll()
                    .Where(c => c.CourseId == _selectedCourse.Id));
                OnPropertyChanged();;
            }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { 
                _name = value;
                OnPropertyChanged();
            }
        }

        private bool _isPremium;

        public bool IsPremium
        {
            get { return _isPremium; }
            set { 
                _isPremium = value;
                OnPropertyChanged();
            }
        }

        private ICommand _chapterAddCommand;

        public ICommand ChaperAddCommand
        {
            get { 
                if (_chapterAddCommand is null)
                {
                    _chapterAddCommand = new RelayCommand(p => { return this.SelectedCourse != null && !String.IsNullOrEmpty(this.Name); },
                        p =>
                        {
                            HandleChapterAddCommand();
                        });
                }
                return _chapterAddCommand; 
            }
        }

        private void HandleChapterAddCommand()
        {
            var allChapters = _chapterRepository.GetAll();
            var chapter = new Chapter()
            {
                Name = this.Name,
                IsPremium = this.IsPremium,
                CourseId = this.SelectedCourse.Id,
                Id = allChapters.Any() ? allChapters.Max(c => c.Id) + 1 : 0,
            };
            Chapters.Add(chapter);
            OnPropertyChanged("Chapters");
            _chapterRepository.SaveAll(Chapters);
            this.Name = String.Empty;
            this.IsPremium = false;
        }


        public event PropertyChangedEventHandler? PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
