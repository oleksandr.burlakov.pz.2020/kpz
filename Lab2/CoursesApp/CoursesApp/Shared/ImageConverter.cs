﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace CoursesApp.Shared
{
    internal class ImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var tabContent = ((TabContent)value).ToString();
            var path = new Uri(@$"E:\Personal\University\3 Grade\КПЗ\Lab2\CoursesApp\CoursesApp\Assets\Icons\{tabContent}.png");
            return new BitmapImage(path);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}