﻿namespace CodeFirstDb
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPremium { get; set; }
        public ICollection<Chapter> Chapters { get; set; }
        public int NumberOfChapters 
        {
            get
            {
                return Chapters.Count;
            }
        }
    }
}
