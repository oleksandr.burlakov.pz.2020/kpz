﻿using CodeFirstDb;
using Courses.Data.Interfaces;

namespace Courses.Data.Implementation
{
    public class ChapterRepository : GeneralRepository<Chapter>, IChapterRepository
    {
        public override void Add(Chapter item)
        {
            using (var context = new CoursesDbContext())
            {
                context.Chapters.Add(item);
                context.SaveChanges();
            }
        }

        public override void Delete(int id)
        {
            using (var context = new CoursesDbContext())
            {
                var item = context.Chapters.FirstOrDefault(c => c.Id == id);
                if (item != null)
                {
                    context.Chapters.Remove(item);
                    context.SaveChanges();
                }
            }
        }

        public override ICollection<Chapter> GetAll()
        {
            using (var context = new CoursesDbContext())
            {
                return context.Chapters.ToList();
            }
        }

        public override Chapter GetById(int id)
        {
            using (var context = new CoursesDbContext())
            {
                return context.Chapters.FirstOrDefault(c => c.Id == id);
            }
        }

        public void RemoveRange(ICollection<Chapter> chapters)
        {
            using (var context = new CoursesDbContext())
            {
                var chapterIds = chapters.Select(c => c.Id).ToList();
                var items = context.Chapters
                    .Where(c => chapterIds.Contains(c.Id))
                    .ToList();
                context.Chapters.RemoveRange(items);
                context.SaveChanges();
            }
        }

        public override void Update(Chapter item)
        {
            using (var context = new CoursesDbContext())
            {
                var chapter = context.Chapters.FirstOrDefault(c => c.Id == item.Id);
                if (chapter != null)
                {
                    chapter.Name = item.Name;
                    chapter.IsPremium = item.IsPremium;
                    chapter.CourseId = item.CourseId;
                    context.SaveChanges();
                }
            }
        }
    }
}
