﻿using CodeFirstDb;
using Courses.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Courses.Data.Implementation
{
    public class CourseRepository : GeneralRepository<Course>, ICourseRepository
    {
        public override void Add(Course item)
        {
            using (var context = new CoursesDbContext())
            {
                context.Courses.Add(item);
                context.SaveChanges();
            }
        }

        public override void Delete(int id)
        {
            using (var context = new CoursesDbContext())
            {
                var item = context.Courses.FirstOrDefault(c => c.Id == id);
                if (item != null)
                {
                    context.Courses.Remove(item);
                    context.SaveChanges();
                }
            }
        }

        public override ICollection<Course> GetAll()
        {
            using (var context = new CoursesDbContext())
            {
                return context.Courses.ToList();
            }
        }

        public override Course GetById(int id)
        {
            using (var context = new CoursesDbContext())
            {
                return context.Courses
                    .Include(c => c.Chapters)
                    .FirstOrDefault(c => c.Id == id);
            }
        }

        public override void Update(Course item)
        {
            using (var context = new CoursesDbContext())
            {
                var course = context.Courses.FirstOrDefault(c => c.Id == item.Id);
                if (course != null)
                {
                    course.Name = item.Name;
                    course.Description = item.Description;
                    course.IsPremium = item.IsPremium;
                    context.SaveChanges();
                }
            }
        }
    }
}
