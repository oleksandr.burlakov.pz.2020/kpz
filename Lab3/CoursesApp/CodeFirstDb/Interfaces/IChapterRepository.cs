﻿using CodeFirstDb;
using System;
namespace Courses.Data.Interfaces
{
    public interface IChapterRepository : IGeneralRepository<Chapter>
    {
        public void RemoveRange(ICollection<Chapter> chapters);
    }
}
