﻿using CodeFirstDb;

namespace Courses.Data.Interfaces
{
    public interface ICourseRepository : IGeneralRepository<Course>
    {
    }
}
