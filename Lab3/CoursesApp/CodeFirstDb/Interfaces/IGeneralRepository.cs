﻿using System;
namespace Courses.Data.Interfaces
{
    public interface IGeneralRepository<T> where T : class
    {
        public ICollection<T> GetAll();
        public T GetById(int id);
        public void Add(T item);
        public void Delete(int id);
        public void Update(T item); 
    }
}
