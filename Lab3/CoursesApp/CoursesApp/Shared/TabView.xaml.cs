﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CoursesApp.Shared
{
    /// <summary>
    /// Interaction logic for TabView.xaml
    /// </summary>
    public partial class TabView : UserControl
    {
        public TabContent ChapterContent { get; set; } = TabContent.Chapter;
        public TabContent CourseContent { get; set; } = TabContent.Course;
        public ICommand MoveToChapterCommand
        {
            get { return (ICommand)GetValue(MoveToChapterCommandProperty); }
            set { SetValue(MoveToChapterCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MoveToChapterCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MoveToChapterCommandProperty =
            DependencyProperty.Register("MoveToChapterCommand", typeof(ICommand), typeof(TabView));

        public ICommand MoveToCourseCommand
        {
            get { return (ICommand)GetValue(MoveToCourseCommandProperty); }
            set { SetValue(MoveToCourseCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MoveToCourseCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MoveToCourseCommandProperty =
            DependencyProperty.Register("MoveToCourseCommand", typeof(ICommand), typeof(TabView));

        public TabView()
        {
            InitializeComponent();
        }
    }
}
