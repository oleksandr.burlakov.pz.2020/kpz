﻿using DbFirst.Models;

namespace Courses.Data.Interfaces
{
    public interface ICourseRepository : IGeneralRepository<Course>
    {
    }
}
