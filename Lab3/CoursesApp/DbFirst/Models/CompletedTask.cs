﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class CompletedTask
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; } = null!;
        public DateTime AccomplishDate { get; set; }
        public int TaskId { get; set; }

        public virtual Task Task { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
