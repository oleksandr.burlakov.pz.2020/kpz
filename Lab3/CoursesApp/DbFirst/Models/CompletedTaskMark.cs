﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class CompletedTaskMark
    {
        public int Id { get; set; }
        public int Mark { get; set; }
        public string? Comment { get; set; }
        public int CompleteTeskId { get; set; }
        public int AuditorId { get; set; }
        public DateTime ReviewDate { get; set; }

        public virtual User Auditor { get; set; } = null!;
        public virtual CompletedTask CompleteTesk { get; set; } = null!;
    }
}
