﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class LessonType
    {
        public LessonType()
        {
            Lessons = new HashSet<Lesson>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;

        public virtual ICollection<Lesson> Lessons { get; set; }
    }
}
