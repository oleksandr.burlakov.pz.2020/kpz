﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class Task
    {
        public Task()
        {
            CompletedTasks = new HashSet<CompletedTask>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public bool IsMarkable { get; set; }
        public int LessonId { get; set; }
        public DateTime? LastDate { get; set; }

        public virtual Lesson Lesson { get; set; } = null!;
        public virtual ICollection<CompletedTask> CompletedTasks { get; set; }
    }
}
