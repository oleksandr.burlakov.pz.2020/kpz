﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public class Course
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsPremium { get; set; }
        public ICollection<Chapter> Chapters { get; set; }
        public int NumberOfChapters { get
            {
                return this.Chapters?.Count ?? 0;
            }
        }
    }
}
