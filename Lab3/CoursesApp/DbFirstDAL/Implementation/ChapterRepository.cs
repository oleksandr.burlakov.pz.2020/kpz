﻿using Courses.Data.Interfaces;
using System.Data.SqlClient;

namespace Courses.Data.Implementation
{
    public class ChapterRepository : GeneralRepository<Chapter>, IChapterRepository
    {
        public ChapterRepository(string connectionString) : base(connectionString)
        {
        }
        public override void Add(Chapter item)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"INSERT INTO [dbo].[Chapter] VALUES
                       (@Id,
                       @Name,
                       @IsPremium,
                       @CourseId)", connection);
                command.Parameters.AddWithValue("@Id", item.Id);
                command.Parameters.AddWithValue("@Name", item.Name);
                command.Parameters.AddWithValue("@IsPremium", item.IsPremium);
                command.Parameters.AddWithValue("@CourseId", item.CourseId);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public override void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"DELETE FROM [dbo].[Chapter] 
                        WHERE Id = @Id", connection);
                command.Parameters.AddWithValue("@Id", id);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public override ICollection<Chapter> GetAll()
        {
            List<Chapter> chapters = new List<Chapter>();
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"SELECT * FROM [dbo].[Chapter]", connection);
                try
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            chapters.Add(new Chapter()
                            {
                                Id = (int)reader[0],
                                Name = (string)reader[1],
                                IsPremium = (bool)reader[2],
                                CourseId = (int)reader[3],
                            });
                        }

                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return chapters;
        }

        public override Chapter GetById(int id)
        {
            Chapter chapter = null;
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"SELECT * FROM [dbo].[Chapter] WHERE Id = @Id", connection);
                command.Parameters.AddWithValue("@Id", id);
                try
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            chapter = new Chapter()
                            {
                                Id = (int)reader[0],
                                Name = (string)reader[1],
                                IsPremium = (bool)reader[2],
                                CourseId = (int)reader[3],
                            };
                        }

                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return chapter;
        }

        public void RemoveRange(ICollection<Chapter> chapters)
        {
            var ids = chapters.Select(x => x.Id).ToList();
            foreach (var id in ids)
            {
                this.Delete(id);
            }
        }

        public override void Update(Chapter item)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"UPDATE [dbo].[Chapter] 
                        SET Name = @Name,
                            IsPremium = @IsPremium,
                            CourseId = @CourseId
                        WHERE Id = @Id", connection);
                command.Parameters.AddWithValue("@Id", item.Id);
                command.Parameters.AddWithValue("@Name", item.Name);
                command.Parameters.AddWithValue("@IsPremium", item.IsPremium);
                command.Parameters.AddWithValue("@CourseId", item.CourseId);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
