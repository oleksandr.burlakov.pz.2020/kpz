﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public enum Roles : int
    {
        User,
        Admin,
        Teacher
    }
}
