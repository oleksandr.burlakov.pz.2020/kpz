﻿using System.Runtime.Serialization;

namespace Courses.Data
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public DateTime BirthDate { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Role { get; set; }
        public ICollection<Course> Courses { get; set; }
    }
}
