﻿using CodeFirstDb;
using Courses.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Courses.Data.Implementation
{
    public class ChapterRepository : GeneralRepository<Chapter>, IChapterRepository
    {
        public override async Task Add(Chapter item)
        {
            using (var context = new CoursesDbContext())
            {
                await context.Chapters.AddAsync(item);
                await context.SaveChangesAsync();
            }
        }

        public override async Task Delete(int id)
        {
            using (var context = new CoursesDbContext())
            {
                var item = await context.Chapters.FirstOrDefaultAsync(c => c.Id == id);
                if (item != null)
                {
                    context.Chapters.Remove(item);
                    await context.SaveChangesAsync();
                }
            }
        }

        public override async Task<ICollection<Chapter>> GetAll()
        {
            using (var context = new CoursesDbContext())
            {
                return await context.Chapters.ToListAsync();
            }
        }

        public override async Task<Chapter> GetById(int id)
        {
            using (var context = new CoursesDbContext())
            {
                return await context.Chapters
                    .FirstOrDefaultAsync(c => c.Id == id);
            }
        }

        public async Task RemoveRange(ICollection<Chapter> chapters)
        {
            using (var context = new CoursesDbContext())
            {
                var chapterIds = chapters.Select(c => c.Id).ToList();
                var items = await context.Chapters
                    .Where(c => chapterIds.Contains(c.Id))
                    .ToListAsync();
                context.Chapters.RemoveRange(items);
                await context.SaveChangesAsync();
            }
        }

        public override async Task Update(Chapter item)
        {
            using (var context = new CoursesDbContext())
            {
                var chapter = await context.Chapters.FirstOrDefaultAsync(c => c.Id == item.Id);
                if (chapter != null)
                {
                    chapter.Name = item.Name;
                    chapter.IsPremium = item.IsPremium;
                    chapter.CourseId = item.CourseId;
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
