﻿using CodeFirstDb;
using Courses.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Courses.Data.Implementation
{
    public class CourseRepository : GeneralRepository<Course>, ICourseRepository
    {
        public override async Task Add(Course item)
        {
            using (var context = new CoursesDbContext())
            {
                await context.Courses.AddAsync(item);
                await context.SaveChangesAsync();
            }
        }

        public override async Task Delete(int id)
        {
            using (var context = new CoursesDbContext())
            {
                var item = await context.Courses.FirstOrDefaultAsync(c => c.Id == id);
                if (item != null)
                {
                    context.Courses.Remove(item);
                    await context.SaveChangesAsync();
                }
            }
        }

        public override async Task<ICollection<Course>> GetAll()
        {
            using (var context = new CoursesDbContext())
            {
                return await context.Courses
                    .Include(c => c.Chapters)
                    .ToListAsync();
            }
        }

        public override async Task<Course> GetById(int id)
        {
            using (var context = new CoursesDbContext())
            {
                return await context.Courses
                    .Include(c => c.Chapters)
                    .FirstOrDefaultAsync(c => c.Id == id);
            }
        }

        public override async Task Update(Course item)
        {
            using (var context = new CoursesDbContext())
            {
                var course = await context.Courses.FirstOrDefaultAsync(c => c.Id == item.Id);
                if (course != null)
                {
                    course.Name = item.Name;
                    course.Description = item.Description;
                    course.IsPremium = item.IsPremium;
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
