﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization;

namespace Courses.Data.Implementation
{
    public abstract class GeneralRepository<T> : IGeneralRepository<T> where T : class
    {
        public GeneralRepository()
        {
        }

        public abstract Task Add(T item);

        public abstract Task Delete(int id);

        public abstract Task<ICollection<T>> GetAll();

        public abstract Task<T> GetById(int id);

        public abstract Task Update(T item);
    }
}
