﻿using CodeFirstDb;
using System;
namespace Courses.Data.Interfaces
{
    public interface IChapterRepository : IGeneralRepository<Chapter>
    {
        public Task RemoveRange(ICollection<Chapter> chapters);
    }
}
