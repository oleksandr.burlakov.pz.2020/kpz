﻿using System;
namespace Courses.Data.Interfaces
{
    public interface IGeneralRepository<T> where T : class
    {
        public Task<ICollection<T>> GetAll();
        public Task<T> GetById(int id);
        public Task Add(T item);
        public Task Delete(int id);
        public Task Update(T item); 
    }
}
