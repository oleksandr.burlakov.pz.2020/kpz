﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CodeFirstDb.Migrations
{
    public partial class ChapterIsPremiumColumnAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPremium",
                table: "Chapters",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPremium",
                table: "Chapters");
        }
    }
}
