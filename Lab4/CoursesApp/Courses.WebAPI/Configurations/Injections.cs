﻿using Courses.Data.Implementation;
using Courses.Data.Interfaces;

namespace Courses.WebAPI.Configurations
{
    public static class Injections
    {
        public static void UseCustomServices(this IServiceCollection collection)
        {
            collection.AddTransient<IChapterRepository, ChapterRepository>();
            collection.AddTransient<ICourseRepository, CourseRepository>();
            collection.AddAutoMapper(typeof(MapperProfile));
        }
    }
}
