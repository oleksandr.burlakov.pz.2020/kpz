﻿using AutoMapper;
using CodeFirstDb;
using Courses.WebAPI.ViewModels.Chapters;
using Courses.WebAPI.ViewModels.Courses;

namespace Courses.WebAPI.Configurations
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Course, CourseViewModel>()
                .ForMember(d => d.NumberOfChapters, opt => opt.MapFrom(s => s.NumberOfChapters));
            CreateMap<CourseInsertViewModel, Course>();
            CreateMap<ChapterInsertViewModel, Chapter>();
            CreateMap<Chapter, ChapterViewModel>();
        }
    }
}
