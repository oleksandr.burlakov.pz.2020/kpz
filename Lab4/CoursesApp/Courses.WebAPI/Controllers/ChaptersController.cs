﻿using AutoMapper;
using CodeFirstDb;
using Courses.Data.Interfaces;
using Courses.WebAPI.ViewModels.Chapters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Courses.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChaptersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IChapterRepository _chapterRepository;
        public ChaptersController(IMapper mapper, IChapterRepository chapterRepository)
        {
            _mapper = mapper;
            _chapterRepository = chapterRepository;
        }

        [HttpGet("get-all")]
        public async Task<IActionResult> GetAll()
        {
            var chapter = await _chapterRepository.GetAll();
            var chapterView = _mapper.Map<ICollection<ChapterViewModel>>(chapter);
            return Ok(chapterView);
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var chapter = await _chapterRepository.GetById(id);
            return Ok(_mapper.Map<ChapterViewModel>(chapter));
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody] ChapterInsertViewModel model)
        {
            var chapter = _mapper.Map<Chapter>(model);
            await _chapterRepository.Add(chapter);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update([FromQuery] int id, [FromBody] ChapterInsertViewModel model)
        {
            var chapter = _mapper.Map<Chapter>(model);
            chapter.Id = id;
            await _chapterRepository.Update(chapter);
            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete([FromQuery] int id)
        {
            await _chapterRepository.Delete(id);
            return NoContent();
        }
    }
}
