﻿using AutoMapper;
using CodeFirstDb;
using Courses.Data.Interfaces;
using Courses.WebAPI.ViewModels.Courses;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Courses.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICourseRepository _coursesRepository;
        public CoursesController(IMapper mapper, ICourseRepository courseRepository)
        {
            _mapper = mapper;
            _coursesRepository = courseRepository;
        }

        [HttpGet("get-all")]
        [SwaggerOperation(Summary = "Get all courses", Description = "Get all courses with nubmer of chaprets that they contains")]
        [SwaggerResponse(200, "Everything works fine")]
        [SwaggerResponse(500, "Problem with accessing data from database")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ICollection<CourseViewModel>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll()
        {
            var courses = await _coursesRepository.GetAll();
            var coursesView = _mapper.Map<ICollection<CourseViewModel>>(courses);
            return Ok(coursesView);
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var course = await _coursesRepository.GetById(id);
            return Ok(_mapper.Map<CourseViewModel>(course));
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody] CourseInsertViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var course = _mapper.Map<Course>(model);
            await _coursesRepository.Add(course);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update([FromQuery] int id, [FromBody] CourseInsertViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var course = _mapper.Map<Course>(model);
            course.Id = id;
            await _coursesRepository.Update(course);
            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete([FromQuery] int id)
        {
            await _coursesRepository.Delete(id);
            return NoContent();
        }
    }
}
