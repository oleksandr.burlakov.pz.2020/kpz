﻿namespace Courses.WebAPI.ViewModels.Chapters
{
    public class ChapterInsertViewModel
    {
        public int CourseId { get; set; }
        public string Name { get; set; }
        public bool IsPremium { get; set; }
    }
}
