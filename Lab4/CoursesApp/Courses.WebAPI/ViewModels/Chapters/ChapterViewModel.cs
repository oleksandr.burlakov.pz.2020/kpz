﻿namespace Courses.WebAPI.ViewModels.Chapters
{
    public class ChapterViewModel
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string Name { get; set; }
        public bool IsPremium { get; set; }
    }
}
