﻿using System.ComponentModel.DataAnnotations;

namespace Courses.WebAPI.ViewModels.Courses
{
    public class CourseInsertViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsPremium { get; set; }
    }
}
