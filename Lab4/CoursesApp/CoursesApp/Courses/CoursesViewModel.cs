﻿using DbFirst.Models;
using DbFirst;
//using CodeFirstDb;
using Courses.Data;
using Courses.Data.Implementation;
using Courses.Data.Interfaces;
using CoursesApp.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CoursesApp.Courses
{
    internal class CoursesViewModel : INotifyPropertyChanged
    {
        ICourseRepository _courseRepository;
        IChapterRepository _chapterRepository;
        public CoursesViewModel()
        {
            this._courseRepository = new CourseRepository();// (Constants.ConnectionString);
            this._chapterRepository = new ChapterRepository();// (Constants.ConnectionString);
            ReloadCourses();
        }

        private void ReloadCourses()
        {
            var courses = _courseRepository.GetAll()
                .GroupJoin(_chapterRepository.GetAll(), c => c.Id, ch => ch.CourseId, (c, ch) => new
                {
                    Id = c.Id,
                    Description = c.Description,
                    IsPremium = c.IsPremium,
                    Name = c.Name,
                    Chapter = ch,
                })
                .SelectMany(c => c.Chapter.DefaultIfEmpty(), (c,ch) => new
                {
                    Id = c.Id,
                    Description = c.Description,
                    IsPremium = c.IsPremium,
                    Name = c.Name,
                    Chapter = ch
                })
                .GroupBy(c => c.Id)
                .Select(g => new Course()
                {
                    Id = g.Key,
                    Name = g.First().Name,
                    Chapters = g.Any(i => i.Chapter != null) ? g.Select(i => i.Chapter).ToList() : new List<Chapter>(),
                    Description = g.First().Description,
                    IsPremium = g.First().IsPremium
                });
            this.Courses = new ObservableCollection<Course>(courses);
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }
        private string _description;
        private bool _isPremium;

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }
        public bool IsPremium
        {
            get => _isPremium;
            set
            {
                _isPremium = value;
                OnPropertyChanged();
            }
        }

        private ICommand _addNewCourse;
        private ObservableCollection<Course> _courses;

        public ICommand AddNewCourse
        {
            get
            {
                if (_addNewCourse is null)
                {
                    _addNewCourse = new RelayCommand(
                        p => !String.IsNullOrEmpty(this.Name) && !String.IsNullOrEmpty(this.Description),
                        p => AddNewCourseHandler()
                        );
                }
                return _addNewCourse;
            }
        }

        private void AddNewCourseHandler()
        {
            var course = new Course()
            {
                Id = this.Courses.Any() ? this.Courses.Max(c => c.Id) + 1 : 0,
                Description = this.Description,
                Name = this.Name,
                IsPremium = this.IsPremium
            };
            Courses.Add(course);
            _courseRepository.Add(course);
            this.Description = String.Empty;
            this.Name = String.Empty;
            this.IsPremium = false;
        }

        public ObservableCollection<Course> Courses { get => _courses;
            set
            {
                _courses = value;
                OnPropertyChanged();
            }
        }

        private ICommand _editCourseCommand;

        public ICommand EditCourseCommand
        {
            get { 
                if (_editCourseCommand is null)
                {
                    _editCourseCommand = new RelayCommand(p => true,
                        p =>
                        {
                            var id = (int)p;
                            var editWindow = new EditCourse(id);
                            editWindow.Closed += (object e, EventArgs args) => ReloadCourses();
                            editWindow.Show();
                        });
                }
                return _editCourseCommand; 
            }
        }

        private ICommand _deleteCourseCommand;

        public ICommand DeleteCourseCommand
        {
            get { 
                if (_deleteCourseCommand is null)
                {
                    _deleteCourseCommand = new RelayCommand(p => true,
                        p =>
                        {
                            var id = (int)p;
                            var course = this.Courses.FirstOrDefault(c => c.Id == id);
                            if (course is not null)
                            {
                                var chapters = _chapterRepository.GetAll().Where(ch => ch.CourseId == course.Id).ToList();
                                _chapterRepository.RemoveRange(chapters);
                                this.Courses.Remove(course);
                                this._courseRepository.Delete(id);
                                OnPropertyChanged("Courses");
                            }
                        });
                }
                return _deleteCourseCommand; 
            }
        }



        public event PropertyChangedEventHandler? PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
