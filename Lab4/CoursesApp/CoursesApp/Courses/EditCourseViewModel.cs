﻿using Courses.Data.Implementation;
using Courses.Data.Interfaces;
using CoursesApp.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CoursesApp.Courses
{
    internal class EditCourseViewModel : INotifyPropertyChanged
    {
        private readonly ICourseRepository _courseRepository = new CourseRepository();// (Constants.ConnectionString);
        private int _courseId;

        public int CourseId { 
            get
            {
                return _courseId;
            }
            set
            {
                _courseId = value;
                OnPropertyChanged();
                var course = _courseRepository.GetById(_courseId);
                if (course != null)
                {
                    this.Name = course.Name;
                    this.Description = course.Description;
                    this.IsPremium = course.IsPremium;
                }
            }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { 
                _name = value;
                OnPropertyChanged();
            }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { 
                _description = value;
                OnPropertyChanged();
            }
        }

        private bool _isPremium;

        public bool IsPremium
        {
            get { return _isPremium; }
            set { 
                _isPremium = value;
                OnPropertyChanged();
            }
        }

        private ICommand _editCourseCommand;

        public ICommand EditCourseCommand
        {
            get {
                if (_editCourseCommand == null)
                {
                    _editCourseCommand = new RelayCommand(p =>
                    {
                        return !String.IsNullOrEmpty(Name);
                    }, p => EditCourseCommandHanlde(p));
                }
                return _editCourseCommand; 
            }
        }

        private void EditCourseCommandHanlde(object property)
        {
            var course = _courseRepository.GetById(_courseId);
            if (course != null)
            {
                course.Name = this.Name;
                course.Description = this.Description;
                course.IsPremium = this.IsPremium;
                _courseRepository.Update(course);
            }
            var window = (Window)property;
            if (window is not null)
            {
                window.Close();
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
