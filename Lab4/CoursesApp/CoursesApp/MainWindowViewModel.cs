﻿using CoursesApp.Chapters;
using CoursesApp.Courses;
using CoursesApp.Shared;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace CoursesApp
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        public object CurrentViewModel { get; set; }

        public MainWindowViewModel()
        {
            CurrentViewModel = new CoursesViewModel();
            MoveToCourses = new RelayCommand(p => CurrentViewModel.GetType() != typeof(CoursesViewModel),
                        p =>
                        {
                            CurrentViewModel = new CoursesViewModel();
                            OnPropertyChanged("CurrentViewModel");
                        }
                    );

            MoveToChapter = new RelayCommand(p => CurrentViewModel.GetType() != typeof(ChaptersViewModel),
                p =>
                {
                    CurrentViewModel = new ChaptersViewModel();
                    OnPropertyChanged("CurrentViewModel");
                }
            );
        }

        public ICommand MoveToCourses { get; set; }

        public ICommand MoveToChapter { get; set; }
        public event PropertyChangedEventHandler? PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
