﻿using Courses.Data.Interfaces;
using System.Runtime.Serialization;

namespace Courses.Data.Implementation
{
    public abstract class GeneralRepository<T> : IGeneralRepository<T> where T : class
    {
        public GeneralRepository()
        {
        }

        public abstract void Add(T item);

        public abstract void Delete(int id);

        public abstract ICollection<T> GetAll();

        public abstract T GetById(int id);

        public abstract void Update(T item);
    }
}
