﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class Course
    {
        public Course()
        {
            Chapters = new HashSet<Chapter>();
            UserCourses = new HashSet<UserCourse>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public bool IsPremium { get; set; }

        public virtual ICollection<Chapter> Chapters { get; set; }
        public virtual ICollection<UserCourse> UserCourses { get; set; }
        public int NumberOfChapters
        { 
            get { return Chapters.Count; }
        }
    }
}
