﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class Lesson
    {
        public Lesson()
        {
            InversePreviousLesson = new HashSet<Lesson>();
            Tasks = new HashSet<Task>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int LessonTypeId { get; set; }
        public int? PreviousLessonId { get; set; }
        public int ChapterId { get; set; }
        public bool IsPremium { get; set; }
        public string Content { get; set; } = null!;

        public virtual Chapter Chapter { get; set; } = null!;
        public virtual LessonType LessonType { get; set; } = null!;
        public virtual Lesson? PreviousLesson { get; set; }
        public virtual ICollection<Lesson> InversePreviousLesson { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}
