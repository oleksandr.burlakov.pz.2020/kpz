﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class Subscription
    {
        public Subscription()
        {
            UserSubscriptions = new HashSet<UserSubscription>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Details { get; set; }
        public int Duration { get; set; }
        public decimal Price { get; set; }

        public virtual ICollection<UserSubscription> UserSubscriptions { get; set; }
    }
}
