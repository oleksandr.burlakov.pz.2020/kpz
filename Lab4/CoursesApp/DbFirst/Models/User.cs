﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class User
    {
        public User()
        {
            CompletedTasks = new HashSet<CompletedTask>();
            UserCourses = new HashSet<UserCourse>();
            UserSubscriptions = new HashSet<UserSubscription>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Password { get; set; } = null!;
        public DateTime BirthDate { get; set; }
        public string Email { get; set; } = null!;
        public string Role { get; set; } = null!;

        public virtual ICollection<CompletedTask> CompletedTasks { get; set; }
        public virtual ICollection<UserCourse> UserCourses { get; set; }
        public virtual ICollection<UserSubscription> UserSubscriptions { get; set; }
    }
}
