﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models
{
    public partial class UserSubscription
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int SubscriptionId { get; set; }
        public DateTime RegistrationDate { get; set; }

        public virtual Subscription Subscription { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
