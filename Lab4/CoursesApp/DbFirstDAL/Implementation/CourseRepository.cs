﻿using Courses.Data.Interfaces;
using System.Data.SqlClient;

namespace Courses.Data.Implementation
{
    public class CourseRepository : GeneralRepository<Course>, ICourseRepository
    {
        public CourseRepository(string connectionString) : base(connectionString)
        {
        }

        public override void Add(Course item)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"INSERT INTO [dbo].[Course] VALUES
                       (@Id,
                       @Name,
                       @Description,
                       @IsPremium)", connection);
                command.Parameters.AddWithValue("@Id", item.Id);
                command.Parameters.AddWithValue("@Name", item.Name);
                command.Parameters.AddWithValue("@Description", item.Description);
                command.Parameters.AddWithValue("@IsPremium", item.IsPremium);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public override void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"DELETE FROM [dbo].[Course] 
                        WHERE Id = @Id", connection);
                command.Parameters.AddWithValue("@Id", id);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public override ICollection<Course> GetAll()
        {
            List<Course> courses = new List<Course>();
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"SELECT * FROM [dbo].[Course]", connection);
                try
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            courses.Add(new Course()
                            {
                                Id = (int)reader[0],
                                Name = (string)reader[1],
                                Description = (string)reader[2],
                                IsPremium = (bool)reader[3]
                            });
                        }

                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return courses;
        }

        public override Course GetById(int id)
        {
            Course course = null;
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"SELECT * FROM [dbo].[Course] WHERE Id = @Id", connection);
                command.Parameters.AddWithValue("@Id", id);
                try
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            course = new Course()
                            {
                                Id = (int)reader[0],
                                Name = (string)reader[1],
                                Description = (string)reader[2],
                                IsPremium = (bool)reader[3]
                            };
                        }

                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return course;
        }

        public override void Update(Course item)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(@"UPDATE [dbo].[Course] 
                        SET Name = @Name,
                            Description = @Description,
                            IsPremium = @IsPremium
                        WHERE Id = @Id", connection);
                command.Parameters.AddWithValue("@Id", item.Id);
                command.Parameters.AddWithValue("@Name", item.Name);
                command.Parameters.AddWithValue("@Description", item.Description);
                command.Parameters.AddWithValue("@IsPremium", item.IsPremium);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
