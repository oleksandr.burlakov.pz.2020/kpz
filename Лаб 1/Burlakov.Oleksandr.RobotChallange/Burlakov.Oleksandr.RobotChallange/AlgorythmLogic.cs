﻿using Robot.Common;
using System.Collections.Generic;

namespace Burlakov.Oleksandr.RobotChallange
{
    public abstract class AlgorythmLogic
    {
        protected readonly Robot.Common.Robot _movingRobot;
        protected readonly IList<Robot.Common.Robot> _robots;
        protected readonly Map _map;
        public AlgorythmLogic(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map)
        {
            _map = map;
            _movingRobot = movingRobot; 
            _robots = robots;
        }
        public abstract RobotCommand Run();
    }
}
