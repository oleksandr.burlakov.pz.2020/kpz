﻿using Robot.Common;
using RobotChallenge;
using System.Collections.Generic;
using System.Windows;

namespace Burlakov.Oleksandr.RobotChallange
{
    public class BurlakovAlgorythm : IRobotAlgorithm
    {
        public static RobotCommand command;
        public string Author => "Oleksandr Burlakov";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            var isTeleportable = TryParseToMainWindow(out var mainWindow);
            AlgorythmLogic algorythm = new RegularLogic(movingRobot, robots, map, this.Author);
            if (isTeleportable)
            {
                algorythm = new GlitchAlgorythm(movingRobot, robots, map, mainWindow, this.Author);
            }
            return algorythm.Run();
        }

        private bool TryParseToMainWindow(out MainWindow window)
        {
            window = null;
            var windows = Application.Current.Windows;
            foreach (var item in windows)
            {
                if (item is MainWindow)
                {
                    window = (MainWindow)item;
                    return true;
                }
            }
            return false;
        }

    }
}
