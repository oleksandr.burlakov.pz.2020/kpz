﻿using Robot.Common;
using RobotChallenge;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Burlakov.Oleksandr.RobotChallange
{
    public class GlitchAlgorythm : AlgorythmLogic
    {
        private readonly string _author;
        private readonly MainWindow _mainWindow;

        public GlitchAlgorythm(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map)
            : base(movingRobot, robots, map)
        {
        }
        public GlitchAlgorythm(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map,
            MainWindow mainWindow, string author)
            : base(movingRobot, robots, map)
        {
            _mainWindow = mainWindow;
            _author = author;
        }

        public override RobotCommand Run()
        {
            TurnOffMovement(_map);
            Position newPosition = FindBestStation(_movingRobot, _map, _robots);
            var myRobotsCount = _robots.Where(x => x.OwnerName == _author).Count();
            TeleportRobot(newPosition);
            if ((_movingRobot.Energy > 300) && (myRobotsCount < _map.Stations.Count && myRobotsCount < 100))
            {
                TurnOnMovement(_map, _robots.Count());
                return new CreateNewRobotCommand();
            }
            return new CollectEnergyCommand();
        }

        public void TurnOnMovement(Map map, int numberOfRobots)
        {
            int coordinates = (int)Math.Sqrt(numberOfRobots) + 1;
            map.MaxPozition.X = coordinates;
            map.MaxPozition.Y = coordinates;
        }

        public void TurnOffMovement(Map map)
        {
            map.MaxPozition.X = 1;
            map.MaxPozition.Y = 1;
        }

        private void TeleportRobot(Position newPosition)
        {
            var place = _mainWindow.RobotPlace
                .Select(rb => new KeyValuePair<Position, RobotControl>(rb.Key, rb.Value))
                .Where(rb => rb.Key.X == _movingRobot.Position.X && rb.Key.Y == _movingRobot.Position.Y)
                .FirstOrDefault();
            var prevPosition = place.Key;
            _mainWindow.RobotPlace.Remove(place.Key);
            place.Key.X = newPosition.X;
            place.Key.Y = newPosition.Y;
            var alreadyTaken = _mainWindow.RobotPlace.TryGetValue(place.Key, out RobotControl robotControl);
            if (alreadyTaken)
            {
                var alreadyTakenPositions = _mainWindow.RobotPlace.Select(r => r.Key);
                var otherRobotNewPosition = new Position();
                var positionFound = false;
                for (int x = 0; x < 100 && !positionFound; ++x)
                {
                    for (int y = 0; y < 100; ++y)
                    {
                        if (!alreadyTakenPositions.Any(p => p.X == x && place.Key.Y == y))
                        {
                            otherRobotNewPosition.X = x;
                            otherRobotNewPosition.Y = y;
                            positionFound = true;
                            break;
                        }
                    }
                }
                _mainWindow.RobotPlace.Remove(place.Key);
                _mainWindow.RobotPlace.Add(otherRobotNewPosition, robotControl);
                _mainWindow.SetAnimation(robotControl, place.Key, otherRobotNewPosition, false);
            }
            _mainWindow.RobotPlace.Add(place.Key, place.Value);
            _mainWindow.SetAnimation(place.Value, prevPosition, place.Key, false);
        }

        public Position FindBestStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation best = null;
            int energy = int.MinValue;
            foreach (var station in map.Stations)
            {
                if (Shared.IsStationFree(station, movingRobot, robots))
                {
                    if (energy < station.Energy)
                    {
                        best = station;
                        energy = station.Energy;
                    }
                }
            }
            return best == null ? null : best.Position;
        }
    }
}
