﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Burlakov.Oleksandr.RobotChallange
{
    public class RegularLogic : AlgorythmLogic
    {
        private readonly string _name;
        public RegularLogic(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map)
            : base(movingRobot, robots, map)
        {
        }

        public RegularLogic(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map,
            string author)
            : base(movingRobot, robots, map)
        {
            _name = author;
        }

        public override RobotCommand Run()
        {
            int energyCollectingRadius = 2;
            EnergyStation station = FindNearesStation(_movingRobot, _map, _robots);
            var stationPosition = station?.Position;
            var nearestRobot = FindNearesEnemy(_robots, _movingRobot.Position);
            var myRobotsCount = _robots.Where(x => x.OwnerName == _name).Count();
            if (stationPosition == null)
                return null;
            if (IsInCollectingRadius(_movingRobot.Position, stationPosition, energyCollectingRadius))
            {
                if ((_movingRobot.Energy > 100) && (myRobotsCount < _map.Stations.Count && myRobotsCount < 100))
                {
                    return new CreateNewRobotCommand();
                }
                else
                {
                    return new CollectEnergyCommand();
                }
            }
            else
            {
                return CalculateMovement(nearestRobot, station, stationPosition);
            }
        }

        public RobotCommand CalculateMovement(Robot.Common.Robot nearestRobot, EnergyStation station, Position stationPosition)
        {
            var energyEffectAfterEnemySteal =
                -CalculateLoss(nearestRobot.Position, _movingRobot.Position) + (nearestRobot.Energy * 0.1);
            var stationEffect = -CalculateLoss(stationPosition, _movingRobot.Position) + (station.Energy % 500);
            var enemyCanCollect = IsInCollectingRadius(nearestRobot.Position, stationPosition, 2);
            var aimPosition = stationPosition;
            if (enemyCanCollect || energyEffectAfterEnemySteal > stationEffect)
            {
                aimPosition = nearestRobot.Position;
            }
            if (CalculateLoss(aimPosition, nearestRobot.Position) > 81)
            {
                var x = (aimPosition.X - _movingRobot.Position.X) / 2;
                var y = (aimPosition.Y - _movingRobot.Position.Y) / 2;
                aimPosition.X = _movingRobot.Position.X + x;
                aimPosition.Y = _movingRobot.Position.Y + y;
            }
            _map.MaxPozition.X = aimPosition.X + 1;
            _map.MaxPozition.Y = aimPosition.Y + 1;
            return new MoveCommand() { NewPosition = aimPosition };
        }

        public Robot.Common.Robot FindNearesEnemy(IList<Robot.Common.Robot> robots, Position robotPosition) 
        {
            var nearestRobot = robots
                .Where(r => r.OwnerName != this._name)
                .OrderBy(r => Shared.FindDistance(r.Position, robotPosition))
                .FirstOrDefault();
            return nearestRobot;
        }

        public bool IsInCollectingRadius(Position robotPosition, Position stationPosition, int radius)
        {
            var distance = Math.Abs(robotPosition.X - stationPosition.X) + Math.Abs(robotPosition.Y - stationPosition.Y);
            if (distance <= radius)
            {
                return true;
            }
            return false;
        }

        public Robot.Common.EnergyStation FindNearesStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            var bestDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (Shared.IsStationFree(station, movingRobot, robots))
                {
                    var stationDistance = Shared.FindDistance(station.Position, movingRobot.Position);
                    if (bestDistance > stationDistance)
                    {
                        nearest = station;
                        bestDistance = stationDistance;
                    }
                }
            }
            return nearest == null ? null : nearest;
        }

        public int Min2D(int x1, int x2)
        {
            int[] source = new int[3]
            {
                (int)Math.Pow(x1 - x2, 2.0),
                (int)Math.Pow(x1 - x2 + 100, 2.0),
                (int)Math.Pow(x1 - x2 - 100, 2.0)
            };
            return source.Min();
        }

        public int CalculateLoss(Position p1, Position p2)
        {
            return Min2D(p1.X, p2.X) + Min2D(p1.Y, p2.Y);
        }
    }
}
