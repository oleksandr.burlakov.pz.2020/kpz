﻿using Burlakov.Oleksandr.RobotChallange;
using NUnit.Framework;
using Robot.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotChallange.Tests
{
    [TestFixture]
    internal class GlitchAlgorythmTest
    {
        [Test]
        public void IfStationIsTakenByMyRobotsReturnAnotherFreeStation()
        {
            var ownerName = "Burlakov";
            var myRobotPosition = new Position(5, 5);
            var enemyRobotPosition = new Position(20, 20);
            var returnPosition = new Position(10, 10);
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation()
                    {
                        Energy = 1000,
                        Position = new Position(100,100)
                    },
                    new EnergyStation()
                    {
                        Energy = 10,
                        Position = new Position(10,10)
                    }
                }
            };
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                OwnerName = ownerName,
                Position = myRobotPosition
            };
            var robotLists = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "some",
                    Energy = 100,
                    Position = enemyRobotPosition
                },
                new Robot.Common.Robot()
                {
                    OwnerName = ownerName,
                    Energy = 100,
                    Position = new Position(100,100)
                }
            };
            var algorythm = new GlitchAlgorythm(null, null, null, null, ownerName);
            var position = algorythm.FindBestStation(myRobot, map, robotLists);
            Assert.AreEqual(position, returnPosition);
        }

        [Test]
        public void IfStationIsFreeFindWithMoreEnergy()
        {
            var ownerName = "Burlakov";
            var myRobotPosition = new Position(5, 5);
            var enemyRobotPosition = new Position(20, 20);
            var returnPosition = new Position(100, 100);
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation()
                    {
                        Energy = 1000,
                        Position = new Position(100,100)
                    },
                    new EnergyStation()
                    {
                        Energy = 10,
                        Position = new Position(5,5)
                    }
                }
            };
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                OwnerName = ownerName,
                Position = myRobotPosition
            };
            var robotLists = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "some",
                    Energy = 100,
                    Position = enemyRobotPosition
                }
            };
            var algorythm = new GlitchAlgorythm(null, null, null, null, ownerName);
            var position = algorythm.FindBestStation(myRobot, map, robotLists);
            Assert.AreEqual(position, returnPosition);
        }

        [Test]
        public void IfNoStationShouldReturnNull()
        {
            var ownerName = "Burlakov";
            var myRobotPosition = new Position(5, 5);
            var enemyRobotPosition = new Position(20, 20);
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                OwnerName = ownerName,
                Position = myRobotPosition
            };
            var robotLists = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "some",
                    Energy = 100,
                    Position = enemyRobotPosition
                }
            };
            var algorythm = new GlitchAlgorythm(null, null, null, null, ownerName);
            var position = algorythm.FindBestStation(myRobot, map, robotLists);
            Assert.IsNull(position);
        }

        [Test]
        public void CheckTurnOffMovements()
        {
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
            };
            var newPosition = new Position(1, 1);
            var algorythm = new GlitchAlgorythm(null, null, null);
            algorythm.TurnOffMovement(map);
            Assert.AreEqual(map.MaxPozition, newPosition);
        }

        [TestCaseSource(typeof(GlitchAlgorythmDataCases), nameof(GlitchAlgorythmDataCases.TurnOnMovementTestCases))]
        public void CheckTurnOnMovements(Map map, int robotsCount, Position newPosition)
        {
            var algorythm = new GlitchAlgorythm(null, null, null);
            algorythm.TurnOnMovement(map, robotsCount);
            Assert.AreEqual(map.MaxPozition, newPosition);
        }
    }

    public class GlitchAlgorythmDataCases
    {
        public static IEnumerable TurnOnMovementTestCases
        {
            get
            {
                yield return new TestCaseData(new Map()
                {
                    MaxPozition = new Position(100, 100),
                    MinPozition = new Position(0, 0),
                }, 25, new Position(6,6));
                yield return new TestCaseData(new Map()
                {
                    MaxPozition = new Position(100, 100),
                    MinPozition = new Position(0, 0),
                }, 10, new Position(4,4));
            }
        }
    }
}
