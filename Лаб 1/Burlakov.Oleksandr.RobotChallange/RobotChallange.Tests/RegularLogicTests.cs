﻿using Burlakov.Oleksandr.RobotChallange;
using NUnit.Framework;
using Robot.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RobotChallange.Tests
{
    [TestFixture]
    internal class RegularLogicTests
    {
        [Test]
        public void IfThereAreNoStationShouldReturnNull()
        {
            var myRobotPosition = new Position(10, 10);
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 10,
                OwnerName = "Burlakov",
                Position = myRobotPosition
            };
            var allRobots = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "Some",
                    Energy = 200,
                    Position = new Position(40,40)
                }
            };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() {}
            };

            var algorythm = new RegularLogic(myRobot, allRobots, map, "Burlakov");
            var command = algorythm.Run();
            Assert.IsNull(command);
        }

        [Test]
        public void IfInNotStationRadiusShouldReturnMoveCommand()
        {
            var myRobotPosition = new Position(10, 10);
            var energyStationPosition = new Position(50, 50);
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 10,
                OwnerName = "Burlakov",
                Position = myRobotPosition
            };
            var allRobots = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "Some",
                    Energy = 200,
                    Position = new Position(40,40)
                }
            };
            var energyStation = new EnergyStation()
            {
                Position = energyStationPosition,
                Energy = 1000
            };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() {
                    energyStation,
                    new EnergyStation() {
                        Energy = 100,
                        Position = new Position(0,0),
                    }
                }
            };

            var algorythm = new RegularLogic(myRobot, allRobots, map, "Burlakov");
            var command = algorythm.Run();
            Assert.That(command, Is.TypeOf<MoveCommand>());
        }

        [Test]
        public void IfInStationRadiusShouldCollectEnergy()
        {
            var myRobotPosition = new Position(49, 50);
            var energyStationPosition = new Position(50, 50);
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 10,
                OwnerName = "Burlakov",
                Position = myRobotPosition
            };
            var allRobots = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "Some",
                    Energy = 200,
                    Position = new Position(40,40)
                }
            };
            var energyStation = new EnergyStation()
            {
                Position = energyStationPosition,
                Energy = 1000
            };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() {
                    energyStation,
                    new EnergyStation() {
                        Energy = 100,
                        Position = new Position(0,0),
                    }
                }
            };

            var algorythm = new RegularLogic(myRobot, allRobots, map, "Burlakov");
            var command = algorythm.Run();
            Assert.That(command, Is.TypeOf<CollectEnergyCommand>());
        }

        [Test]
        public void IfMyRobotsNumberIsNotSmallerThanStationNumberShouldNotCreateNewRobot()
        {
            var myRobotPosition = new Position(49, 50);
            var energyStationPosition = new Position(50, 50);
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 500,
                OwnerName = "Burlakov",
                Position = myRobotPosition
            };
            var allRobots = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "Some",
                    Energy = 200,
                    Position = new Position(40,40)
                }
            };
            var energyStation = new EnergyStation()
            {
                Position = energyStationPosition,
                Energy = 1000
            };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() {
                    energyStation
                }
            };

            var algorythm = new RegularLogic(myRobot, allRobots, map, "Burlakov");
            var command = algorythm.Run();
            Assert.That(command, Is.Not.TypeOf<CreateNewRobotCommand>());
        }

        [Test]
        public void IfInStationRadiusAndCanCreateRobotShouldCreateRobot()
        {
            var myRobotPosition = new Position(49, 50);
            var energyStationPosition = new Position(50, 50);
            var myRobot = new Robot.Common.Robot()
            {
                Energy = 500,
                OwnerName = "Burlakov",
                Position = myRobotPosition
            };
            var allRobots = new List<Robot.Common.Robot>()
            {
                myRobot,
                new Robot.Common.Robot()
                {
                    OwnerName = "Some",
                    Energy = 200,
                    Position = new Position(40,40)
                }
            };
            var energyStation = new EnergyStation()
            {
                Position = energyStationPosition,
                Energy = 1000
            };
            var map = new Map()
            {
                MaxPozition = new Position(100,100),
                MinPozition = new Position(0,0),
                Stations = new List<EnergyStation>() { 
                    energyStation,
                    new EnergyStation() {
                        Energy = 100,
                        Position = new Position(0,0),
                    }
                }
            };

            var algorythm = new RegularLogic(myRobot, allRobots, map, "Burlakov");
            var command = algorythm.Run();
            Assert.That(command, Is.TypeOf<CreateNewRobotCommand>());
        }

        [Test]
        public void IfDistanceToStationIsLargeShouldSplitInTwice()
        {
            var myRobotPosition = new Position(0, 0);
            var stationPosition = new Position(10, 0);
            var enemyPosition = new Position(10, 10);
            var splittedInHalfPosition = new Position(5, 0);
            var myRobot = new Robot.Common.Robot()
            {
                Position = myRobotPosition,
                Energy = 200
            };
            var nearestEnemy = new Robot.Common.Robot()
            {
                Position = enemyPosition,
                Energy = 10
            };
            var energyStation = new EnergyStation() { Energy = 500, Position = stationPosition };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() { energyStation }
            };
            var algorythm = new RegularLogic(myRobot, null, map, "Burlakov");
            var moveCommand = (MoveCommand)algorythm.CalculateMovement(nearestEnemy, energyStation, stationPosition);
            Assert.That(moveCommand, Is.Not.Null);
            Assert.AreEqual(moveCommand.NewPosition, splittedInHalfPosition);
        }


        [Test]
        public void IfEnemyEnergyStealIsMoreEfficientThanStation()
        {
            var myRobotPosition = new Position(0, 0);
            var stationPosition = new Position(5, 5);
            var enemyPosition = new Position(0, 1);
            var myRobot = new Robot.Common.Robot()
            {
                Position = myRobotPosition,
                Energy = 200
            };
            var nearestEnemy = new Robot.Common.Robot()
            {
                Position = enemyPosition,
                Energy = 1000
            };
            var energyStation = new EnergyStation() { Energy = 500, Position = stationPosition };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() { energyStation }
            };
            var algorythm = new RegularLogic(myRobot, null, map, "Burlakov");
            var moveCommand = (MoveCommand)algorythm.CalculateMovement(nearestEnemy, energyStation, stationPosition);
            Assert.That(moveCommand, Is.Not.Null);
            Assert.AreEqual(moveCommand.NewPosition, enemyPosition);
        }

        [Test]
        public void IfEnemyCanCollectEnergyShouldHitHim()
        {
            var myRobotPosition = new Position(0, 0);
            var stationPosition = new Position(5, 5);
            var enemyPosition = new Position(5,6);
            var myRobot = new Robot.Common.Robot()
            {
                Position = myRobotPosition,
                Energy = 200
            };
            var nearestEnemy = new Robot.Common.Robot()
            {
                Position = enemyPosition,
                Energy = 10
            };
            var energyStation = new EnergyStation() { Energy = 500, Position = stationPosition };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() { energyStation }
            };
            var algorythm = new RegularLogic(myRobot, null, map, "Burlakov");
            var moveCommand = (MoveCommand)algorythm.CalculateMovement(nearestEnemy, energyStation, stationPosition);
            Assert.That(moveCommand, Is.Not.Null);
            Assert.AreEqual(moveCommand.NewPosition, enemyPosition);
        }

        [Test]
        public void IfStationCloserCalculateMovementShouldReturnStationPosition()
        {
            var myRobotPosition = new Position(0, 0);
            var stationPosition = new Position(5, 5);
            var enemyPosition = new Position(10, 10);
            var myRobot = new Robot.Common.Robot()
            {
                Position = myRobotPosition,
                Energy = 200
            };
            var nearestEnemy = new Robot.Common.Robot()
            {
                Position = enemyPosition,
                Energy = 200
            };
            var energyStation = new EnergyStation() { Energy = 500, Position = stationPosition };
            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>() { energyStation }
            };
            var algorythm = new RegularLogic(myRobot, null, map, "Burlakov");
            var moveCommand = (MoveCommand)algorythm.CalculateMovement(nearestEnemy, energyStation, stationPosition);
            Assert.That(moveCommand, Is.Not.Null);
            Assert.AreEqual(moveCommand.NewPosition, stationPosition);
        }

        [Test]
        public void FindNearestEnemyDontReturnMyRobots()
        {
            var algorythm = new RegularLogic(null, null, null, "Burlakov");
            var robots = new List<Robot.Common.Robot>()
                {
                    new Robot.Common.Robot()
                    {
                        OwnerName = "Burlakov",
                        Position = new Position(0,0),
                    },
                    new Robot.Common.Robot()
                    {
                        OwnerName = "Burlakov",
                        Position = new Position(2,2)
                    }
                };
            var myPosition = new Position(1, 1);
            var result = algorythm.FindNearesEnemy(robots.ToList(), myPosition);
            Assert.IsNull(result);
        }

        [TestCaseSource(typeof(RegularLogicData), nameof(RegularLogicData.FindNearestEnemyTestCase))]
        public void FindNearestEnemyReturnOnlyEnemyRobots(ICollection<Robot.Common.Robot> robots, Position myPosition, Position returnPosition)
        {
            var algorythm = new RegularLogic(null, null, null, "Burlakov");
            var result = algorythm.FindNearesEnemy(robots.ToList(), myPosition);
            Assert.AreEqual(result.Position, returnPosition);
        }

        [TestCaseSource(typeof(RegularLogicData), nameof(RegularLogicData.IsInCollectingRadiusTestCase))]
        public void TestIsInCollectingRadius(Position position1, Position position2, int radius, bool result)
        {
            var logic = new RegularLogic(null, null, null, null);
            var isInRadius = logic.IsInCollectingRadius(position1, position2, radius);
            Assert.AreEqual(result, isInRadius);
        }

        [TestCaseSource(typeof(RegularLogicData), nameof(RegularLogicData.CalculateLossTestCase))]
        public void TestCalculateLossCalculation(Position position1, Position position2, int cost)
        {
            var logic = new RegularLogic(null, null, null, null);
            var lossCost = logic.CalculateLoss(position1, position2);
            Assert.AreEqual(cost, lossCost);
        }

        [TestCaseSource(typeof(RegularLogicData), nameof(RegularLogicData.Min2DTestCase))]
        public void TestMin2d(int x1, int x2, int result)
        {
            var logic = new RegularLogic(null, null, null);
            var distance = logic.Min2D(x1, x2);
            Assert.AreEqual(result, distance);
        }
    }

    public class RegularLogicData
    {
        public static IEnumerable FindNearestEnemyTestCase
        {
            get
            {
                yield return new TestCaseData(new List<Robot.Common.Robot>()
                {
                    new Robot.Common.Robot()
                    {
                        Position = new Position(5,5),
                        OwnerName = "Some"
                    },
                    new Robot.Common.Robot()
                    {
                        Position = new Position(0,0),
                        OwnerName = "Some"
                    }
                }, new Position(-1, -1), new Position(0,0));
                yield return new TestCaseData(new List<Robot.Common.Robot>()
                {
                    new Robot.Common.Robot()
                    {
                        OwnerName = "Burlakov",
                        Position = new Position(1,1)
                    },
                    new Robot.Common.Robot()
                    {
                        OwnerName = "Some",
                        Position = new Position(5,5)
                    },
                }, new Position(0, 0), new Position(5, 5));
            }
        }

        public static IEnumerable IsInCollectingRadiusTestCase
        {
            get
            {
                yield return new TestCaseData(new Position(0,0), new Position(1,1), 2, true);
                yield return new TestCaseData(new Position(0,0), new Position(2,0), 2, true);
                yield return new TestCaseData(new Position(0,0), new Position(-2, 0), 2, true);
                yield return new TestCaseData(new Position(0,0), new Position(2, 2), 2, false);
                yield return new TestCaseData(new Position(0,0), new Position(-2, -2), 2, false);
                yield return new TestCaseData(new Position(0,0), new Position(0, 0), 0, true);
                yield return new TestCaseData(new Position(0,0), new Position(0, 1), 0, false);
            }
        }

        public static IEnumerable CalculateLossTestCase
        {
            get
            {
                yield return new TestCaseData(new Position(0, 100), new Position(100, 0), 0);
                yield return new TestCaseData(new Position(5, 5), new Position(0, 0), 50);
                yield return new TestCaseData(new Position(5, 5), new Position(15, 0), 125);
                yield return new TestCaseData(new Position(0, 0), new Position(100, 0), 0);
            }
        }

        public static IEnumerable Min2DTestCase
        {
            get
            {
                yield return new TestCaseData(100, 0, 0);
                yield return new TestCaseData(0, 100, 0);
                yield return new TestCaseData(6, 2, 16);
                yield return new TestCaseData(0, 10, 100);
            }
        }
    }
}
