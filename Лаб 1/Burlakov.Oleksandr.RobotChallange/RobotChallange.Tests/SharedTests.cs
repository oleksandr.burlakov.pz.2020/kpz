using Burlakov.Oleksandr.RobotChallange;
using NUnit.Framework;
using Robot.Common;
using System.Collections;
using System.Collections.Generic;

namespace RobotChallange.Tests
{
    [TestFixture]
    public class SharedTests
    {
        [TestCaseSource(typeof(SharedDataClass), nameof(SharedDataClass.IsStationFreeData))]
        public void CheckIfStationIsNotFreeOnlyForOtherRobots(EnergyStation station, Robot.Common.Robot robot, List<Robot.Common.Robot> robots, bool result)
        {
            var isStationFree = Shared.IsStationFree(station, robot, robots);
            Assert.AreEqual(isStationFree, result);
        }

        [TestCaseSource(typeof(SharedDataClass), nameof(SharedDataClass.IsCellFreeData))]
        public void CheckIfCellIsNotFreeOnlyForOtherRobots(Position position, Robot.Common.Robot robot, List<Robot.Common.Robot> robots, bool result)
        {
            var isCellFree = Shared.IsCellFree(position, robot, robots);
            Assert.AreEqual(isCellFree, result);
        }


        [TestCaseSource(typeof(SharedDataClass), nameof(SharedDataClass.FindDistanceData))]
        public void CheckFindDistanceCorrectness(Position one, Position two, int distance)
        {
            var calculatedDistance = Shared.FindDistance(one, two);
            Assert.AreEqual(calculatedDistance, distance);
        }
    }

    public class SharedDataClass
    {
        public static IEnumerable FindDistanceData
        {
            get
            {
                yield return new TestCaseData(new Position(5, 5), new Position(5, 5), 0);
                yield return new TestCaseData(new Position(0, 0), new Position(5, 0), 25);
                yield return new TestCaseData(new Position(0, 0), new Position(5, 5), 50);
                yield return new TestCaseData(new Position(-5, -5), new Position(5, 5), 200);
            }
        }

        public static IEnumerable IsCellFreeData
        {
            get
            {
                yield return new TestCaseData(new Position(5, 5),
                    new Robot.Common.Robot() { Position = new Position(5, 5) },
                    new List<Robot.Common.Robot>()
                    {
                        new Robot.Common.Robot()
                        {
                            Position = new Position(0,0)
                        },
                        new Robot.Common.Robot()
                        {
                            Position = new Position(-5,-5)
                        }
                    },
                    true);
                yield return new TestCaseData(new Position(0, 0),
                    new Robot.Common.Robot() { Position = new Position(5, 5) },
                    new List<Robot.Common.Robot>()
                    {
                        new Robot.Common.Robot()
                        {
                            Position = new Position(0,0)
                        },
                        new Robot.Common.Robot()
                        {
                            Position = new Position(-5,-5)
                        }
                    },
                    false);
                yield return new TestCaseData(new Position(-5, 5),
                    new Robot.Common.Robot() { Position = new Position(5, 5) },
                    new List<Robot.Common.Robot>()
                    {
                        new Robot.Common.Robot()
                        {
                            Position = new Position(0,0)
                        },
                        new Robot.Common.Robot()
                        {
                            Position = new Position(-5,-5)
                        }
                    },
                    true);
            }
        }

        public static IEnumerable IsStationFreeData
        {
            get
            {
                yield return new TestCaseData(
                    new EnergyStation() { Position = new Position(5, 5) },
                    new Robot.Common.Robot() { Position = new Position(5, 5) },
                    new List<Robot.Common.Robot>()
                    {
                        new Robot.Common.Robot()
                        {
                            Position = new Position(0,0)
                        },
                        new Robot.Common.Robot()
                        {
                            Position = new Position(-5,-5)
                        }
                    },
                    true);
                yield return new TestCaseData(
                    new EnergyStation() { Position = new Position(0,0) },
                    new Robot.Common.Robot() { Position = new Position(5, 5) },
                    new List<Robot.Common.Robot>()
                    {
                        new Robot.Common.Robot()
                        {
                            Position = new Position(0,0)
                        },
                        new Robot.Common.Robot()
                        {
                            Position = new Position(-5,-5)
                        }
                    },
                    false);
                yield return new TestCaseData(
                    new EnergyStation() { Position = new Position(-5, 5) },
                    new Robot.Common.Robot() { Position = new Position(5, 5) },
                    new List<Robot.Common.Robot>()
                    {
                        new Robot.Common.Robot()
                        {
                            Position = new Position(0,0)
                        },
                        new Robot.Common.Robot()
                        {
                            Position = new Position(-5,-5)
                        }
                    },
                    true);
            }
        }
    }
}